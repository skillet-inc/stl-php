<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="./style.css">
  <title>PHPによるフォームの実装</title>
</head>
<body>
  <div class="wrap">
    <h1 class="title">
      お問い合わせ内容の確認
    </h1>
    <p>
入力内容をご確認のうえ、送信ボタンをクリックして下さい。
数日以内に担当者より折り返しのご連絡をいたします。
    </p>
    <form action="./submit.php" method="POST" class="form">
      <div class="form__item">
        <label for="plan" class="form__label">ご希望プラン</label>
        <select id="plan" name="plan" required disabled value="<?php echo $_POST['plan']; ?>"class="form__input">
          <option value="スタート" <?php if ( $_POST['plan'] === 'スタート' ) { echo ' selected'; } ?>>スタート</option>
          <option value="ノーマル" <?php if ( $_POST['plan'] === 'ノーマル' ) { echo ' selected'; } ?>>ノーマル</option>
          <option value="アドバンス" <?php if ( $_POST['plan'] === 'アドバンス' ) { echo ' selected'; } ?>>アドバンス</option>
          <option value="その他" <?php if ( $_POST['plan'] === 'その他' ) { echo ' selected'; } ?>>その他</option>
        </select>
      </div>
      <div class="form__item">
        <label for="name" class="form__label">お名前</label>
        <input type="text" name="name" placeholder="山田 花子" required disabled value="<?php echo htmlspecialchars($_POST['name']); ?>" class="form__input" />
      </div>
      <div class="form__item">
        <label for="ruby" class="form__label">フリガナ</label>
        <input type="text" name="ruby" placeholder="ヤマダ ハナコ" required disabled value="<?php echo htmlspecialchars($_POST['ruby']); ?>" class="form__input" />
      </div>
      <div class="form__item">
        <label for="email" class="form__label">メールアドレス</label>
        <input type="email" name="email" placeholder="sample@example.com" required disabled value="<?php echo htmlspecialchars($_POST['email']); ?>" class="form__input" />
      </div>
      <div class="form__item">
        <label for="detail" class="form__label">お問い合わせ内容</label>
        <textarea name="detail" placeholder="例）イベントの利用を検討しています。メールでパンフレットなどをご送付いただきたいです。" disabled class="form__input form__input__textarea"><?php echo htmlspecialchars($_POST['detail']); ?></textarea>
      </div>
      <p>
        プライバシーポリシー
      </p>
      <p class="privacy">
Lo    rem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
      <div class="form__item form__item__privacy">
        <input type="checkbox" id="privacy" name="privacy" required disabled <?php if ( $_POST['privacy'] === 'on' ) { echo ' checked'; } ?> />
        <label for="privacy" class="form__label">プライバシーポリシーに同意する</label>
      </div>
      <div class="form__item form__item__submit">
        <input type="submit" value="送信" class="form__submit" />
      </div>
    </form>
  </div>
</body>
</html>

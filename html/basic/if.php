<?php
$variable = 1;
?>
<h1>
  if文による条件分岐
</h1>
<p>
変数の内容は
<?php
if ($variable === 1) {
  echo "1です";
} else {
  echo "1ではありません";
}
?>
</p>

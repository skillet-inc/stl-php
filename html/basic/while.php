<?php
$tax = 1.1;
$apple = 100;
?>
<h1>
  while文による繰り返し処理
</h1>
りんごの価格
<ul>
<?php
$i = 1;
while ($apple * $tax * $i < 1000) {
  echo "<li>";
  echo $i;
  echo "つ";
  echo $apple * $tax * $i;
  echo "円</li>";
  $i++;
}
?>
</ul>

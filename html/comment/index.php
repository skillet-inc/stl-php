<?php
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>
    コメント機能
  </title>
  <link rel="stylesheet" href="./style.css">
</head>
<body>
  <div class="wrap">
    <form action="./post.php" method="POST" class="form">
      <h1>
        コメント機能
      </h1>
      <div class="form__item">
        <label for="name" class="form__label">投稿者名</label>
        <input id="name" type="text" name="name"required class="form__input">
      </div>
      <div class="form__item">
        <label for="text" class="form__label">投稿内容</label>
        <textarea id="text" name="text" required class="form__textarea"></textarea>
      </div>
      <div class="form__item form__item__submit">
        <input type="submit" value="送信する" class="form__submit">
      </div>
    </form>
    <div class="comment">
      <ol class="comment__list">
        <li class="comment__item">
          <div class="comment__meta">
            <p class="comment__username">
              @投稿者名
            </p>
            <p class="comment__date">
              投稿日時
            </p>
          </div>
          <p class="comment__content">
            投稿内容
          </p>
        </li>
      </ol>
    </div>
  </div>
</body>
</html>
